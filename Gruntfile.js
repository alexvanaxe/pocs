module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    // Configure the copy task to move files from the development to production folders
    copy: {
      target: {
        files: {
          'build/': ['src/html/**']
        }
      }
    },    
  });

  grunt.loadNpmTasks('grunt-contrib-copy');

  // Define your tasks here
  grunt.registerTask('default', ['copy']);

};
